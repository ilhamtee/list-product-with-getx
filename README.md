# LIST PRODUCTS APPS

<img src = "assets/image/overview_ui.jpg">
<p>
A simple list product app built using the GetX State Management, Flutter framework and [DummyJson API](https://dummyjson.com/products).
</p>

## Description

A simple App to show a list of products and some features like filtering and adding items to the Cart Page. All sample products obtained from dummyjson Open API.
<br>

## Getting Started

### Flutter Version

* 3.3.10

### Dependencies

* get
* dio

### Intermezzo
<p>Enjoy the app and feel free to give feedback as there may still be some bugs to fix.</p>

### Executing

* flutter pub get
* flutter run
