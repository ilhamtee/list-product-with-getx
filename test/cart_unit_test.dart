import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/cart/controller/cart_controller.dart';
import 'package:list_item_getx/models/product_model.dart';

void main() {
  late CartContoller cartController;

  setUp(() {
    cartController = CartContoller();
  });

  group('Cart Controller Tests', () {
    test('addToCart should add product to the cart list', () {
      // arrange
      var product = Product(
          id: 1,
          title: 'Product 1',
          price: 10000,
          stock: 5,
          currentQuantity: RxInt(1));

      // act
      cartController.addToCart(product);

      // assert
      expect(cartController.listCartProduct.length, 1);
      expect(cartController.listCartProduct.first.id, product.id);
    });

    test(
        'increaseQuantity should increase the current quantity of the selected product',
        () {
      // arrange
      var product = Product(
          id: 1,
          title: 'Product 1',
          price: 10000,
          stock: 5,
          currentQuantity: RxInt(1));
      cartController.addToCart(product);

      // act
      cartController.increaseQuantity(0);

      // assert
      expect(cartController.listCartProduct[0].currentQuantity.value, 2);
    });

    test(
        'decreaseQuantity should decrease the current quantity of the selected product',
        () {
      // arrange
      var product = Product(
          id: 1,
          title: 'Product 1',
          price: 10000,
          stock: 5,
          currentQuantity: RxInt(2));
      cartController.addToCart(product);

      // act
      cartController.decreaseQuantity(0);

      // assert
      expect(cartController.listCartProduct.first.currentQuantity.value, 1);
    });

    test(
        'removeProductFromCart should remove the selected product from the cart list',
        () {
      // arrange
      var product = Product(
          id: 1,
          title: 'Product 1',
          price: 10000,
          stock: 5,
          currentQuantity: RxInt(1));
      cartController.addToCart(product);

      // act
      cartController.removeProductFromCart(0);

      // assert
      expect(cartController.listCartProduct.length, 0);
    });

    test(
        'clearAllProductsFromCart should reset the current quantity of all products and clear the cart list',
        () {
      // arrange
      var product1 = Product(
          id: 1,
          title: 'Product 1',
          price: 10000,
          stock: 5,
          currentQuantity: RxInt(1));
      var product2 = Product(
          id: 2,
          title: 'Product 2',
          price: 20000,
          stock: 10,
          currentQuantity: RxInt(1));
      cartController.addToCart(product1);
      cartController.addToCart(product2);

      // act
      cartController.clearAllProductsFromCart();

      // assert
      expect(cartController.listCartProduct.length, 0);
      expect(product1.currentQuantity.value, 1);
      expect(product2.currentQuantity.value, 1);
    });
  });
}
