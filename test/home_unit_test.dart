import 'package:flutter_test/flutter_test.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/home/controller/home_controller.dart';
import 'package:list_item_getx/models/product_model.dart';

void main() {
  group('HomeController', () {
    late HomeController homeController;
    late List<Product> sampleProductList;

    setUp(() {
      homeController = HomeController();
      sampleProductList = [
        Product(
            id: 1, title: 'Product 1', price: 100, currentQuantity: RxInt(1)),
        Product(
            id: 2, title: 'Product 2', price: 200, currentQuantity: RxInt(1)),
        Product(
            id: 3, title: 'Product 3', price: 300, currentQuantity: RxInt(1)),
      ];
    });

    test('Fetch Products Test', () async {
      homeController.fecthProducts();

      expect(homeController.mainListProduct.length, greaterThan(0));
      expect(homeController.filteredListProduct.length, greaterThan(0));
      expect(homeController.isLoading.value, false);
    });

    test('filterProduct should filter products based on min and max price', () {
      homeController.mainListProduct.value = sampleProductList;

      homeController.filterProduct(minPrice: 150, maxPrice: 250);

      expect(homeController.filteredListProduct,
          [sampleProductList[1]]); // Only Product 2 meets the filter criteria
    });

    test(
        'resetFilterProduct should reset filteredListProduct to mainListProduct',
        () {
      homeController.mainListProduct.value = sampleProductList;
      homeController.filteredListProduct.value = [sampleProductList[1]];

      homeController.resetFilterProduct();

      expect(homeController.filteredListProduct, sampleProductList);
    });
  });
}
