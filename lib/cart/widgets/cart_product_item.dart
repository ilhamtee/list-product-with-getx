import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/cart/controller/cart_controller.dart';
import 'package:list_item_getx/models/product_model.dart';

class CartProductItem extends StatelessWidget {
  CartProductItem({Key? key, required this.product, required this.indexProduct})
      : super(key: key);

  final Product product;
  final int indexProduct;
  final CartContoller cartContoller = Get.find<CartContoller>();

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(bottom: 16),
      color: Colors.white,
      child: ListTile(
        contentPadding: const EdgeInsets.only(right: 0, left: 16, top: 5),
        title: Text(
          product.title ?? '',
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Wrap(
              children: List.generate(
                  5,
                  (index) => Icon(
                        Icons.star_outlined,
                        size: 12,
                        color: index < product.rating!.toInt()
                            ? Colors.yellow
                            : Colors.grey[400],
                      )),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              '\$${product.price}',
              style: const TextStyle(
                  color: Colors.blue, fontWeight: FontWeight.w500),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                InkWell(
                  onTap: (() => cartContoller.increaseQuantity(indexProduct)),
                  child: Container(
                    width: 20,
                    height: 20,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        color: Colors.blueAccent,
                        borderRadius: BorderRadius.all(Radius.circular(2))),
                    child: const Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5.0,
                ),
                Obx(
                  () => Text(
                    product.currentQuantity.toString(),
                    style: const TextStyle(
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                const SizedBox(
                  width: 5.0,
                ),
                InkWell(
                  onTap: () => cartContoller.decreaseQuantity(indexProduct),
                  child: Container(
                    width: 20,
                    height: 20,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        color: Colors.blueAccent,
                        borderRadius: BorderRadius.all(Radius.circular(2))),
                    child: const Icon(
                      Icons.remove,
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      cartContoller.removeProductFromCart(indexProduct);
                    },
                    icon: const Icon(
                      Icons.delete,
                      color: Colors.red,
                      size: 23,
                    ))
              ],
            )
          ],
        ),
        leading: Image.network(
          product.thumbnail!,
          width: 100,
          height: 150,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
