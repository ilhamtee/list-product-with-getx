import 'package:get/get.dart';
import 'package:list_item_getx/models/product_model.dart';

class CartContoller extends GetxController {
  RxList<Product> listCartProduct = <Product>[].obs;
  RxInt totalPriceProduct = RxInt(0);

  void addToCart(Product product) {
    bool isProductExist = false;

    for (var cartProduct in listCartProduct) {
      if (cartProduct.id == product.id) {
        cartProduct.currentQuantity = cartProduct.currentQuantity + 1;
        isProductExist = true;
        break;
      }
    }

    if (!isProductExist) {
      listCartProduct.add(product);
    }
    calculateTotalPrice();
  }

  void increaseQuantity(int index) {
    if (listCartProduct[index].stock! >
        listCartProduct[index].currentQuantity.value) {
      listCartProduct[index].currentQuantity.value =
          listCartProduct[index].currentQuantity.value + 1;
      calculateTotalPrice();
    }
  }

  void decreaseQuantity(int index) {
    if (listCartProduct[index].currentQuantity.value > 1) {
      listCartProduct[index].currentQuantity.value =
          listCartProduct[index].currentQuantity.value - 1;
      calculateTotalPrice();
    }
  }

  void removeProductFromCart(int index) {
    listCartProduct[index].currentQuantity.value = 1; // reset quantity
    listCartProduct.removeAt(index);
    calculateTotalPrice();
  }

  void clearAllProductsFromCart() {
    for (int i = 0; i < listCartProduct.length; i++) {
      listCartProduct[i].currentQuantity.value = 1;
    }
    listCartProduct.clear();
  }

  void calculateTotalPrice() {
    int newTotalPrice = 0;
    for (var product in listCartProduct) {
      newTotalPrice += (product.currentQuantity.value * product.price!);
    }
    totalPriceProduct.value = newTotalPrice;
  }
}
