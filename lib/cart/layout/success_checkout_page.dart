// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/home/layout/home_page.dart';

class SuccessCheckoutPage extends StatelessWidget {
  const SuccessCheckoutPage({Key? key}) : super(key: key);

  void backToHome() {
    Future.delayed(const Duration(milliseconds: 1500), () {
      Get.offAll(HomePage());
    });
  }

  @override
  Widget build(BuildContext context) {
    backToHome();
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Center(
              child: Icon(
                Icons.check_circle,
                size: 40,
                color: Colors.green,
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Center(
                child: Text(
              'Thank you for your payment.\nYour order with transaction number #123456 has been successfully processed and is being prepared for shipping.',
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.blue),
            ))
          ],
        ),
      ),
    );
  }
}
