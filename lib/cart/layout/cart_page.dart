// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/cart/controller/cart_controller.dart';
import 'package:list_item_getx/cart/layout/success_checkout_page.dart';
import 'package:list_item_getx/cart/widgets/cart_product_item.dart';

class CartPage extends StatelessWidget {
  CartPage({Key? key}) : super(key: key);

  final CartContoller cartContoller = Get.find<CartContoller>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Your Cart"),
        ),
        backgroundColor: Colors.grey[200],
        body: Obx((() {
          if (cartContoller.listCartProduct.isEmpty) {
            return const Center(
              child:
                  Text('No products have been added to your shopping cart yet'),
            );
          } else {
            return Container(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
              child: ListView.builder(
                  itemCount: cartContoller.listCartProduct.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    var product = cartContoller.listCartProduct[index];

                    return CartProductItem(
                      product: product,
                      indexProduct: index,
                    );
                  }),
            );
          }
        })),
        bottomNavigationBar: Obx(() {
          if (cartContoller.listCartProduct.isNotEmpty) {
            return Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: Obx((() => Text(
                          'Total : \$${cartContoller.totalPriceProduct}',
                          style: const TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.bold),
                        )))),
                Container(
                  width: double.maxFinite,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: ElevatedButton(
                    onPressed: (() {
                      cartContoller.clearAllProductsFromCart();
                      Get.to(() => const SuccessCheckoutPage());
                    }),
                    child: const Text('Checkout'),
                  ),
                ),
              ],
            );
          } else {
            return const SizedBox();
          }
        }));
  }
}
