import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/cart/controller/cart_controller.dart';
import 'package:list_item_getx/home/widgets/custom_snackbar.dart';
import 'package:list_item_getx/models/product_model.dart';

class ProductItem extends StatelessWidget {
  ProductItem({
    Key? key,
    required this.product,
  }) : super(key: key);

  final Product product;
  final CartContoller cartContoller = Get.find<CartContoller>();

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.only(bottom: 16),
      child: ListTile(
        title: Text(
          product.title ?? '',
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
        contentPadding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '(${product.stock}) | ${product.category}',
              style: const TextStyle(fontSize: 11),
            ),
            const SizedBox(
              height: 30.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  '\$${product.price}',
                  style: const TextStyle(
                      color: Colors.blue, fontWeight: FontWeight.w500),
                ),
                InkWell(
                  onTap: () {
                    cartContoller.addToCart(product);
                    customSnackbar(
                        context: context,
                        message: 'Success Add Product to Cart');
                  },
                  child: Container(
                    padding:
                        const EdgeInsets.symmetric(vertical: 3, horizontal: 5),
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        color: Color.fromARGB(255, 101, 155, 247)),
                    alignment: Alignment.center,
                    child: const Text(
                      'Add to Cart',
                      style: TextStyle(color: Colors.white, fontSize: 11),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
        leading: Image.network(
          product.thumbnail!,
          width: 100,
          height: 150,
          fit: BoxFit.contain,
        ),
      ),
    );
  }
}
