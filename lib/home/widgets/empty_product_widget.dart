// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/home/controller/home_controller.dart';

class EmptyProductWidget extends StatelessWidget {
  EmptyProductWidget({Key? key}) : super(key: key);

  final HomeController homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text.rich(
            textAlign: TextAlign.center,
            TextSpan(
                text: 'No Products Found',
                style: const TextStyle(color: Colors.black, fontSize: 15),
                children: [
                  const TextSpan(
                    text: '\n or\nTry to',
                  ),
                  TextSpan(
                    text: ' refresh',
                    style: const TextStyle(color: Colors.blue),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        homeController.fecthProducts();
                      },
                  ),
                  const TextSpan(text: ' the page')
                ])));
  }
}
