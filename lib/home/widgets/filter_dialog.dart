// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/home/controller/home_controller.dart';

class FilterDialog extends StatelessWidget {
  final HomeController homeController;
  final Function(int, int) onFilter;

  FilterDialog({Key? key, required this.onFilter, required this.homeController})
      : super(key: key);

  final TextEditingController _minPriceController = TextEditingController();
  final TextEditingController _maxPriceController = TextEditingController();

  int _minPrice = 0;
  int _maxPrice = 0;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          const Text('Filter'),
          InkWell(
              onTap: () => Navigator.pop(context),
              child: const Icon(Icons.clear)),
        ],
      ),
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          TextField(
            controller: _minPriceController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            maxLines: 1,
            onChanged: (value) =>
                _minPrice = int.parse(_minPriceController.text),
            decoration: const InputDecoration(
                labelText: 'Minimum Price',
                hintText: 'Enter minimum price',
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width: 1)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width: 1))),
          ),
          const SizedBox(
            height: 10.0,
          ),
          TextField(
            controller: _maxPriceController,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            maxLines: 1,
            onChanged: (value) =>
                _maxPrice = int.parse(_maxPriceController.text),
            decoration: const InputDecoration(
                labelText: 'Maximum Price',
                hintText: 'Enter maximum price',
                enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width: 1)),
                border: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.blue, width: 1))),
          ),
        ],
      ),
      actions: [
        TextButton(
          onPressed: () {
            homeController.resetFilterProduct();
            Get.back();
          },
          child: const Text('RESET'),
        ),
        ElevatedButton(
          onPressed: () {
            onFilter(_minPrice, _maxPrice);
            Get.back();
          },
          child: const Text('APPLY'),
        ),
      ],
    );
  }
}
