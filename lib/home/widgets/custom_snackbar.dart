import 'package:flutter/material.dart';

void customSnackbar(
    {BuildContext? context,
    String message = '',
    VoidCallback? onPressed,
    Duration duration = const Duration(milliseconds: 1200)}) {
  ScaffoldMessenger.of(context!).showSnackBar(
    SnackBar(
      content: Text(message),
      duration: duration,
      backgroundColor: Colors.blue,
      action: onPressed != null
          ? SnackBarAction(
              label: 'Undo',
              onPressed: onPressed,
            )
          : null,
    ),
  );
}
