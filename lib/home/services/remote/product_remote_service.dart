import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:list_item_getx/helper/constant_url_helper.dart';
import 'package:list_item_getx/models/product_model.dart';

class ProductRemoteService {
  Future<List<Product>> getAllProducts() async {
    List<Product> listProduct = [];
    Dio dio = Dio();
    dio.options.connectTimeout = 5000;

    try {
      var response = await dio
          .get(ConstantUrlHelper.baseUrl + ConstantUrlHelper.productEndpoint);
      if (response.statusCode == 200) {
        for (var product in response.data['products']) {
          listProduct.add(Product.fromJson(product));
        }
      }

      return listProduct;
    } on DioError catch (e) {
      debugPrint(e.toString());
      return listProduct;
    }
  }
}
