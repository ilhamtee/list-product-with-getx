import 'package:get/get.dart';
import 'package:list_item_getx/home/services/remote/product_remote_service.dart';
import 'package:list_item_getx/models/product_model.dart';

class HomeController extends GetxController {
  RxList<Product> mainListProduct = <Product>[].obs;
  RxList<Product> filteredListProduct = <Product>[].obs;
  RxBool isLoading = false.obs;

  void fecthProducts() async {
    isLoading.value = true;
    ProductRemoteService productRemoteService = ProductRemoteService();

    try {
      mainListProduct.value = await productRemoteService.getAllProducts();
      filteredListProduct.value = mainListProduct;
      isLoading.value = false;
    } catch (e) {
      isLoading.value = false;
    }
  }

  void filterProduct({int minPrice = 0, required int maxPrice}) {
    filteredListProduct.value = [];
    filteredListProduct.assignAll(mainListProduct
        .where((product) =>
            product.price! >= minPrice && product.price! <= maxPrice)
        .toList());
  }

  void resetFilterProduct() {
    filteredListProduct.value = [];
    filteredListProduct.assignAll(mainListProduct);
  }

  @override
  void onInit() {
    fecthProducts();
    super.onInit();
  }
}
