// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/cart/layout/cart_page.dart';
import 'package:list_item_getx/home/controller/home_controller.dart';
import 'package:list_item_getx/home/widgets/empty_product_widget.dart';
import 'package:list_item_getx/home/widgets/filter_dialog.dart';
import 'package:list_item_getx/home/widgets/product_item.dart';

class HomePage extends StatelessWidget {
  HomePage({Key? key}) : super(key: key);

  final HomeController homeController = Get.find<HomeController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('List Product'),
          actions: [
            IconButton(
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => FilterDialog(
                            homeController: homeController,
                            onFilter: (minPrice, maxPrice) {
                              homeController.filterProduct(
                                  minPrice: minPrice, maxPrice: maxPrice);
                            },
                          ));
                },
                icon: const Icon(Icons.filter_alt)),
            IconButton(
                onPressed: () => Get.to(() => CartPage()),
                icon: const Icon(Icons.shopping_cart)),
          ],
        ),
        backgroundColor: Colors.grey[200],
        body: Obx((() {
          if (homeController.isLoading.value) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (!homeController.isLoading.value &&
              homeController.filteredListProduct.isEmpty) {
            return EmptyProductWidget();
          } else {
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
              child: ListView.builder(
                  itemCount: homeController.filteredListProduct.length,
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    var product = homeController.filteredListProduct[index];
                    return ProductItem(product: product);
                  }),
            );
          }
        })));
  }
}
