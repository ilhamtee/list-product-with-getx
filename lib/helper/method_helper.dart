class MethodHelper {
  static int getPrice(String price) {
    int outputPrice = 0;

    if (price.isNotEmpty) {
      outputPrice = int.tryParse(price)!;
    }

    return outputPrice;
  }
}
