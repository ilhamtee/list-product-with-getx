class ConstantUrlHelper {
  static const String baseUrl = 'https://dummyjson.com';
  static const String productEndpoint = '/products';
}
