import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:list_item_getx/cart/controller/cart_controller.dart';
import 'package:list_item_getx/home/controller/home_controller.dart';
import 'package:list_item_getx/home/layout/home_page.dart';

void main() {
  Get.put(HomeController());
  Get.put(CartContoller());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Product Apss',
      debugShowCheckedModeBanner: false,
      home: HomePage(),
    );
  }
}
